package ds2.tools.maven.plugins.embeddedtomcat8.impl;

import ds2.tools.maven.plugins.embeddedtomcat8.api.WebappDto;
import org.apache.catalina.Context;
import org.apache.catalina.Host;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.Set;

/**
 * Created by dstrauss on 05.04.17.
 */
@Mojo(name = "start", defaultPhase = LifecyclePhase.PRE_INTEGRATION_TEST)
public class StartTomcat extends AbstractMojo {
    @Parameter(property = "embedded.tc8.httpPort", defaultValue = "18180", required = true)
    private int httpPort;
    @Parameter(property = "embedded.tc8.directory", defaultValue = "${project.build.directory}/tc8", required = true)
    private String directory;
    @Parameter
    private List<WebappDto> webapps;
    @Parameter(property = "embedded.tc8.wait", defaultValue = "false")
    private boolean wait;
    @Parameter(property = "embedded.tc8.props")
    private Properties runProperties;
    private Tomcat tomcat;

    public void execute() throws MojoExecutionException, MojoFailureException {
        try {
            getLog().debug("Using basic tomcat directory " + directory);
            Path tmpDirP = Paths.get(directory).toAbsolutePath().normalize();
            Files.createDirectories(tmpDirP);
            Files.createDirectories(tmpDirP.resolve("webapps"));
            Files.createDirectories(tmpDirP.resolve("work"));
            if (runProperties != null && runProperties.size() > 0) {
                getLog().debug("Using some system properties: " + runProperties);
                Set<String> names = runProperties.stringPropertyNames();
                for (String name : names) {
                    System.setProperty(name, runProperties.getProperty(name));
                }
            }
            getLog().debug("Configuring tomcat..");
            tomcat = new Tomcat();
            tomcat.setPort(httpPort);
            tomcat.setBaseDir(tmpDirP.toString());
            tomcat.enableNaming();
            Host host = tomcat.getHost();
            host.setAutoDeploy(true);
            host.setCreateDirs(true);
            host.setDeployOnStartup(true);
            getLog().debug("Adding webapps..");
            for (WebappDto webappDto : webapps) {
                Path warPath = webappDto.getPathToWar().toPath();
                Context appContext = tomcat.addWebapp(host, webappDto.getContextPath(), warPath.toString());
                File ctxPath = webappDto.getPathToContext();
                if (ctxPath != null) {
                    getLog().debug("Using context from " + ctxPath);
                    //appContext.setConfigFile(getClass().getResource("/masterapi-ctx.xml"));
                    appContext.setConfigFile(ctxPath.toURI().toURL());
                } else {
                    getLog().debug("Using default context");
                }
                if (webappDto.isUseParentClassloader()) {
                    appContext.setParentClassLoader(getClass().getClassLoader());
                }
            }
            getLog().info("Starting server NOW..");
            tomcat.start();
            InstanceRegistry.getInstance().addInstance(tomcat);
            if (wait) {
                getLog().info("Starting endless loop..");
                tomcat.getServer().await();
            }
        } catch (LifecycleException e) {
            throw new MojoExecutionException("Error on the lifecycle!", e);
        } catch (IOException e) {
            throw new MojoExecutionException("IO Error occurred!", e);
        }
        if (tomcat != null) {
            getLog().info("Tomcat still alive.");
        }
    }
}
