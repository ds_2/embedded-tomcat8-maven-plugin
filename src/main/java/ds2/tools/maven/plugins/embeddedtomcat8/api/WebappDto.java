package ds2.tools.maven.plugins.embeddedtomcat8.api;

import lombok.*;

import java.io.File;
import java.io.Serializable;

/**
 * Created by dstrauss on 05.04.17.
 */
@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class WebappDto implements Serializable {
    private String contextPath;
    private File pathToWar;
    private File pathToContext;
    private boolean useParentClassloader = true;

}
