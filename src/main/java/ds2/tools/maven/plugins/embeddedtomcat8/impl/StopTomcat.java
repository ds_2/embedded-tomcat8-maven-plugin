package ds2.tools.maven.plugins.embeddedtomcat8.impl;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * Created by dstrauss on 05.04.17.
 */
@Mojo(name = "stop", defaultPhase = LifecyclePhase.POST_INTEGRATION_TEST)
public class StopTomcat extends AbstractMojo {
    public void execute() throws MojoExecutionException, MojoFailureException {
        InstanceRegistry.getInstance().shutdownAllTomcats();
        getLog().info("Done");
    }
}
