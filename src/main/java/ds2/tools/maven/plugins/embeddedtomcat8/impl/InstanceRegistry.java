package ds2.tools.maven.plugins.embeddedtomcat8.impl;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import java.lang.invoke.MethodHandles;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by dstrauss on 27.04.17.
 */
public final class InstanceRegistry {
    private static final Logger LOGGER = Logger.getLogger(MethodHandles.lookup().lookupClass().getName());
    private static InstanceRegistry instanceRegistry;
    private Set<Tomcat> tomcats;

    private InstanceRegistry() {
        //do nothing :D
        tomcats = new HashSet<>(1);
    }

    public static InstanceRegistry getInstance() {
        if (instanceRegistry == null) {
            instanceRegistry = new InstanceRegistry();
            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    getInstance().shutdownAllTomcats();
                } catch (Exception e) {
                    LOGGER.log(Level.FINE, "Error when stopping all tomcats!", e);
                }
            }));
        }
        return instanceRegistry;
    }

    public void addInstance(Tomcat t) {
        tomcats.add(t);
    }

    public void shutdownAllTomcats() {
        tomcats.forEach(t -> {
            try {
                t.stop();
            } catch (LifecycleException e) {
                LOGGER.log(Level.FINE, "Error when stopping this tomcat!", e);
            }
        });
    }
}
